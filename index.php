<?php
use \Ezy;
include_once('./api/Restaurant.php');
include_once('./api/ReviewForm.php');

try {
    $b = new Bootstrap();
    echo $b->printPage();
} catch (Exception $e) {
    echo "<div class='alert alert-warning'>{$e->getMessage()}</div>";
}
        
class Bootstrap {
    public $user;
    public $restaurant;
    public $theme;
    public $content;
    public $base_url;
    
    public function __construct() {
        $this->base_url;
        $this->restaurant = \Ezy\Ezy::getTestRestaurant();
        $this->user = new \Ezy\User(array(
            'mobile' => '+447723827585', 
            'name' => 'api test user',
            'country_iso' => 'GB',
            'mobile_verified' => 1,
            'secondary_contact' => 'pipra133@yahoo.com',
            "ts" => "2015-02-10 15:07:45",
        ));
        
        $this->theme = $this->getTheme();
    }
    
    public function printPage() {
        if ($this->theme instanceof \Ezy\Theme) {
            $this->restaurant->setTheme($this->theme);            
            include_once($this->_getLayoutFileName($this->theme));
        }
    }
    
    public function getTheme() {
        if ($this->theme instanceof \Ezy\Theme) {
            return $this->theme;
        }
        
        $themeId = (isset($_GET['theme']) ? $_GET['theme'] : 'theme1');
        $theme = \Ezy\Ezy::findTheme($themeId);
                
        if (!$theme instanceof \Ezy\Theme) {
            throw new Exception("$themeId is not found.");
        }
        
        if (!$theme->isValid()) {
            throw new Exception("$themeId failed validation.");
        }
        
        return $this->theme = $theme;
    }
    
    private function _getLayoutFileName() {        
        $fileName = $this->theme->getLayoutFileUrl();
        if (! is_file($fileName)) {
            throw new Exception("Layout file '$fileName' not found for theme {$this->theme->getName()}.");
        }
        
        return $fileName;
    }       
    
    public function getViewScriptName($theme) {
        $scriptName = (isset($_GET['page']) ? $_GET['page'] : 'home');
        $pageNames = \Ezy\Ezy::getPageNames();
        if (false === array_search($scriptName, $pageNames)) {
            $pageNamesString = implode(', ', $pageNames);
            throw new Exception("$scriptName is not a valid page name. Page parameter can only be one of these { $pageNamesString }");
        }
        
        //exceptional case for Home page
        $fileName = $theme->themeDirectoryLocation() . ($scriptName == 'home' ? 'index' : $scriptName) . ".phtml";                
        if (! is_file($fileName)) {
            throw new Exception("$fileName file not found for theme {$theme->getName()}.");
        }
        
        return $fileName;
    } 
    
    /*
    *   To support Zend style layout
    */    
    public function layout() {    
        return new Layout($this);
    }
    
    public function headMeta() {
        $s = new HeadMeta();
        return $s;
    }
    
    public function headScript() {
        $s = new HeadScript();
        return $s;
        
    }
    
    public function headLink() {
        return new HeadLink($this->theme);
    }
    
    public function headTitle() {
        return '';
    }
    
    public function headStyle() {
        return new HeadStyle();
    }
    
    public function baseUrl($link) {
        $link = str_replace('/tr/', "/" . basename(__DIR__) ."/", $link);
        return $this->base_url . $link;
    }
}

class Layout {
    public $content;
    public $bootstrap;
    public $restaurant;
    public $user;
    public $theme;
	public $reviews;
    
    public function __construct($bootstrap) {
        $this->bootstrap = $bootstrap;
        $this->restaurant = $this->bootstrap->restaurant;
        $this->user = $this->bootstrap->user;
        $this->theme = $this->bootstrap->getTheme();
        $this->reviews = new \Ezy\Feed();
        $scriptName = $this->bootstrap->getViewScriptName($this->theme);
        include_once($scriptName);
        $this->content = null; //cheating the Zend way
    }
    
    public function helper() {
        return $this->helpers;
    }
    
    // to make it available on layout even
    public function baseUrl($url) {
        return $this->bootstrap->baseUrl($url);
    }
	
	public function paginationPlain(){
		
	}
    
    public function reviewForm() {
        return new \Ezy\ReviewForm();
    }
    
    public function checkoutBasket() {
        return new \Ezy\CheckoutBasket();
    }
    
    public function recommendButton() {
        return new \Ezy\recommendButton($this->user);
    }
}

class HeadScript {
    public $script_links;
    
    public function __construct() {        
        return "<!--  Meta tags will be here automatically -->";
    }
    
    public function prependFile($link) {
        $this->script_links[] = $link;        
    }
    
    public function __toString() {
        return "<!--  Meta tags will be here automatically -->";
    }
}

class HeadLink {
    public $styles = array();
    public $theme;
    
    public function __construct($theme){
        $this->theme = $theme;
    }
    
    public function appendStylesheet($link) {
        $this->styles[] = $link;
    }
    
    public function __toString() {
        $tmp = array();
        if (isset($_GET['selected_color_scheme'])) {
            $this->styles[] = $_GET['selected_color_scheme'];
        }
        
        foreach($this->styles as $s) {
            $href = $this->theme->getColorSchemeCssFile($s);
            $tmp[] = "<link rel=\"stylesheet\" href=\"$href\">";
        }
        
        return implode(PHP_EOL, $tmp);
    }
}

class HeadStyle {
    public $styles = array();
    
    public function __construct(){
    }
    
    public function setStyle($style) {
        $this->styles = array();
        $this->styles[] = "<style>$style</style>";
    }
    
    public function appendStyle($style) {
        $this->styles[] = "<style>$style</style>";
    }
    
    public function __toString() {
        return implode(PHP_EOL, $this->styles);
    }
}

class HeadMeta {
    public $scripts;
    
    public function __construct(){
        return "<!--  Meta tags will be here automatically -->";
    }
    
    public function __toString() {
        return "<!--  Meta tags will be here automatically -->";
    }
}
