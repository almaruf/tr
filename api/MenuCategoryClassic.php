<?php
namespace Ezy;
include_once('MenuCategory.php');
use \Ezy\MenuCategory as MenuCategory;

class MenuCategoryClassic extends MenuCategory {
    
    public $show_vegetable_price;
    public $show_lamb_price;
    public $show_beef_price; 
    public $show_chicken_lamb_tikka_price;    
    public $show_prawn_price;
    public $show_king_prawn_price;
    public $show_duck_price;    
    public $show_mixed_price;    
    public $show_chicken_tikka_price;    
    public $show_lamb_tikka_price ;    
   
    public function __construct($options = null){
        parent::__construct($options);
    }
        
     
    public function setShowVegetablePrice($value){
        $this->show_vegetable_price = $value;
        return $this;
    }
    public function getShowVegetablePrice(){
        return $this->escape($this->show_vegetable_price);
    }
    
    public function setShowLambPrice($value){
        $this->show_lamb_price = $value;
        return $this;
    }
    public function getShowLambPrice(){
        return $this->escape($this->show_lamb_price);
    }        
     
    public function setShowBeefPrice($value){
        $this->show_beef_price = $value;
        return $this;
    }
    public function getShowBeefPrice(){
        return $this->escape($this->show_beef_price);
    }    
    
    public function setShowChickenLambTikkaPrice($value){
        $this->show_chicken_lamb_tikka_price = $value;
        return $this;
    }
    public function getShowChickenLambTikkaPrice(){
        return $this->escape($this->show_chicken_lamb_tikka_price);
    }
       
    public function setShowChickenTikkaPrice($value){
        $this->show_chicken_tikka_price = $value;
        return $this;
    }
    public function getShowChickenTikkaPrice(){
        return $this->escape($this->show_chicken_tikka_price);
    }     
    
    public function setShowLambTikkaPrice($value){
        $this->show_lamb_tikka_price = $value;
        return $this;
    }
    public function getShowLambTikkaPrice(){
        return $this->escape($this->show_lamb_tikka_price);
    }   
     
    public function setShowPrawnPrice($value){
        $this->show_prawn_price = $value;
        return $this;
    }
    public function getShowPrawnPrice(){
        return $this->escape($this->show_prawn_price);
    }
        
     
    public function setShowKingPrawnPrice($value){
        $this->show_king_prawn_price = $value;
        return $this;
    }
    public function getShowKingPrawnPrice(){
        return $this->escape($this->show_king_prawn_price);
    }
        
     
    public function setShowDuckPrice($value){
        $this->show_duck_price = $value;
        return $this;
    }
    public function getShowDuckPrice(){
        return $this->escape($this->show_duck_price);
    }
        
     
    public function setShowMixedPrice($value){
        $this->show_mixed_price = $value;
        return $this;
    }
    public function getShowMixedPrice(){
        return $this->escape($this->show_mixed_price);
    }
}