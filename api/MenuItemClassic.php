<?php
namespace Ezy;
include_once('MenuItem.php');
use \Ezy\MenuItem as MenuItem;

class MenuItemClassic extends MenuItem{
    
    public $price_lamb;
    public $price_beef;
    public $price_chicken_lamb_tikka;    
    public $price_duck;
    public $price_prawn;
    public $price_king_prawn;
    public $price_vegetable;
    public $price_mixed;
    
    public function __construct($options = null){
        parent::__construct($options);
    }    
    
    public function setPriceLamb ($value){
        $this->price_lamb = $value;
        return $this;
    }
    public function getPriceLamb(){
        return $this->escape($this->price_lamb) ;
    }    
    
    public function setPriceChickenLambTikka ($value){
        $this->price_chicken_lamb_tikka = $value;
        return $this;
    }
    public function getPriceChickenLambTikka(){
        return $this->escape($this->price_chicken_lamb_tikka) ;
    }    
    
    public function setPriceBeef ($value){
        $this->price_beef = $value;
        return $this;
    }
    public function getPriceBeef(){
        return $this->escape($this->price_beef) ;
    }    
    
    public function setPriceDuck ($value){
        $this->price_duck = $value;
        return $this;
    }
    public function getPriceDuck(){
        return $this->escape($this->price_duck) ;
    } 
    
    public function setPriceVegetable ($value){
        $this->price_vegetable = $value;
        return $this;
    }
    public function getPriceVegetable(){
        return $this->escape($this->price_vegetable);
    }    
    
    public function setPricePrawn ($value){
        $this->price_prawn = $value;
        return $this;
    }
    public function getPricePrawn(){
        return $this->escape($this->price_prawn);
    }    
    
    public function setPriceKingPrawn ($value){
        $this->price_king_prawn = $value;
        return $this;
    }
    public function getPriceKingPrawn(){
        return $this->escape($this->price_king_prawn);
    }   
    
    public function setPriceMixed ($value){
        $this->price_mixed = $value;
        return $this;
    }
    public function getPriceMixed(){
        return $this->escape($this->price_mixed) ;
    }    
}