<?php 
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Seo extends Base {

    public $google_site_varification_content;
    public $bing_site_varification_content;    
    public $google_analytics_account_id;
    public $head_keywords;
    public $head_title;
    public $head_description;
    
    public function __construct($options){
        parent::__construct($options);
    }
    
    public function setGoogleSiteVarificationContent($value){
        $this->google_site_varification_content = $value;
        return $this;
    }
    
    public function getGoogleSiteVarificationContent(){
        return $this->escape($this->google_site_varification_content);
    }
    
    
    public function setBingSiteVarificationContent($value){
        $this->bing_site_varification_content = $value;
        return $this;
    }
    
    public function getBingSiteVarificationContent(){
        return $this->escape($this->bing_site_varification_content);
    }
    
    
    public function setGoogleAnalyticsAccountId($value){
        $this->google_analytics_account_id = $value;
        return $this;
    }
    
    public function getGoogleAnalyticsAccountId(){
        return $this->escape($this->google_analytics_account_id);
    }
    
    
    public function setHeadKeywords($value){
        $this->head_keywords = $value;
        return $this;
    }
    
    public function getHeadKeywords(){
        return $this->escape($this->head_keywords);
    }
    /*
    *   Alis of getHeadKeywords()
    */
    public function getKeywords(){
        return $this->getHeadKeywords();
    }
    
    
    
    public function setHeadTitle($value){
        $this->head_title = $value;
        return $this;
    }
    
    public function getHeadTitle(){
        return $this->escape($this->head_title);
    }
    /*
    *   Alis of getHeadTitle()
    */
    public function getTitle(){
        return $this->getHeadTitle();
    }
    
    
    
    public function setHeadDescription($value){
        $this->head_description = $value;
        return $this;
    }
    
    public function getHeadDescription(){
        return $this->escape($this->head_description);
    }
    /*
    *   Alis of getHeadDescription()
    */
    public function getDescription(){
        return $this->getHeadDescription();
    }
}
