<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Video extends Base{

    /**
    *   YOUTUBE video ID
    */
    public $id;   
    
    /**
    *   YOUTUBE video Thumbnail number 
    */
    public $thumbnail_number = 1; // either of these three { 1 , 2, 3}   
    
    /**
    *   showing youtube video title 
    */
    public $title;
    public $description;
    
    /**
    *   Embedded player height
    */
    public $height = 315;
    
    /**
    *   Embedded player width
    */
    public $width = 420;
        
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    public function isPublished(){
        return ($this->getStatus() == Ezy::STATUS_PUBLISHED);
    }    
    
    /**
    *	Get YOUTUBE embedded player HTML for rendering straight to the page
    *
    */
    public function getYoutubePlayer(){
    	if($this->getId()){
			return "<iframe "
            . "width='{$this->getWidth()}' "
            . "height='{$this->getHeight()}' " 
    		. "src='//www.youtube.com/embed/{$this->getId()}' "
            . "frameborder='0'></iframe>";
    	}
    }
    
    
    /**
    *	Get YOUTUBE video thumbnail 
    *
    */
    public function getThumbnail($classes = 'img-thumbnail'){
    	if($this->getThumbnailUrl()){
			return "<img src='{$this->getThumbnailUrl()}' class='$classes' />";
    	}
    } 
    
    /**
    *	Get YOUTUBE video thumbnail URL for the video
    *
    */
    public function getThumbnailUrl(){
    	if($this->getId()){
			return "http://img.youtube.com/vi/{$this->getId()}/{$this->getThumbnailNumber()}.jpg";
    	}
    }   
    
    
    public function setThumbnailNumber($value){
        $this->thumbnail_number = $value;
        return $this;
    }
    public function getThumbnailNumber(){
        return $this->thumbnail_number;
    }
    
    
    public function setId($value){
        $this->id = $value;
        return $this;
    }
    public function getId(){
        return $this->id;
    }
    
    
    public function setTitle($value){
        $this->title = $value;
        return $this;
    }
    public function getTitle(){
        return $this->title;
    }
    
    
    public function setHeight($value){
        $this->height = $value;
        return $this;
    }
    public function getHeight(){
        return $this->height;
    }
    
    
    public function setWidth($value){
        $this->width = $value;
        return $this;
    }
    public function getWidth(){
        return $this->width;
    }
    
    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        return $this->description;
    }
}
