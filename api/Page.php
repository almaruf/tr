<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Page extends Base{

    public $name;
    public $display_name;
    public $title;
    public $description;
    public $keywords;

    public function __construct($options = null){
        if($this->getName()){
            $def = new DefaultPages();
            $this->setOptions($def->getPage($this->getName()));
        }
        parent::__construct($options);
    }

    public function getUri($cms = true, $params = null){
        $ezy = new Ezy();
        if($cms){
            return self::ADMIN_CONTROLLER_RESTAURANT
            . "list-page"
            . "/ref/" . $this->getRestaurantId()
            . "/page-name/" . $this->getName()
            . $this->appendParamsIntoUrl($params);
        }

        return self::CONTROLLER_URL_USER
            . '/' . $this->getName()
            . "/ref/" . $this->getRestaurantId();
    }

    public function getUpdateUri($params = null){
        $ezy = new Ezy();
        return self::ADMIN_CONTROLLER_RESTAURANT
            . 'manage-page'
            . "/ref/" . $this->getRestaurantId()
            . "/page-name/" . $this->getName()
            . $this->appendParamsIntoUrl($params);
    }

    public function getPopulatedForm(){
        $form = new Restaurant_Form_Page();
        $form->populate(get_object_vars($this));
        return $form;
    }

    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function getName(){
        return $this->escape($this->name);
    }

    public function setDisplayName($value){
        $this->display_name = $value;
        return $this;
    }
    public function getDisplayName(){
        if(!$this->display_name && $this->getName()){
            $def = new DefaultPages();
            $page = $def->getPage($this->getName());
            $this->display_name = $page['display_name'];
        }
        return $this->escape($this->display_name);
    }

    public function setTitle($value){
        $this->title = $value;
        return $this;
    }
    public function getTitle(){
        if(!$this->title && $this->getName()){
            $def = new DefaultPages();
            $page = $def->getPage($this->getName());
            $this->title = $page['title'];
        }
        return $this->escape($this->title);
    }

    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        if(!$this->description && $this->getName()){
            $def = new DefaultPages();
            $page = $def->getPage($this->getName());
            $this->description = $page['description'];
        }
        return $this->description;
    }

    public function setKeywords($value){
        $this->keywords = $value;
        return $this;
    }
    public function getKeywords(){
        if(!$this->keywords && $this->getName()){
            $def = new DefaultPages();
            $page = $def->getPage($this->getName());
            $this->keywords = $page['keywords'];
        }
        return $this->escape($this->keywords);
    }
}



/*
    DEFAULT VALUES FOR PAGES

*/

class DefaultPages{
    private $pages = array(
        'videos' => array(
            'display_name' => 'Videos',
            'title' => 'Our Videos',
            'description' => 'Here are few videos for you, enjoy!',
            'keywords' => 'our, videos, our videos, restaurant, videos, restaurant videos, our customer, customers',
        ),
        'photos' => array(
            'display_name' => 'Photos',
            'title' => 'How we look :)',
            'description' => 'Here we have put few photos of our restaurant inside and outside.',
            'keywords' => 'our, photos, our photos, restaurant, photos, restaurant photos, our customer, customers',
        ),
        'about' => array(
            'display_name' => 'About Us',
            'title' => 'Who are we?',
            'description' => 'This page is to describe about our restaurant, with a little details.',
            'keywords' => 'about, our, restaurant, about our restaurant',
        ),
        'menu' => array(
            'display_name' => 'Menus',
            'title' => null,
            'description' => null,
            'keywords' => 'menu, takeaway, takeaway food, in menu, eat in, food, food items',
        ),
        'home' => array(
            'display_name' => 'Home',
            'title' => null,
            'description' => null,
            'keywords' => 'about, our, restaurant, about our restaurant, menu, food, special, treat, good service',
        ),
        'reviews' => array(
            'display_name' => 'Reviews',
            'title' => 'Customer Reviews',
            'description' => 'We love to hear from our customers. Please leave a review here about our food & service. We will take your suggestions to improve the quality of our food & services.',
            'keywords' => 'review, reviews, what, our, customers, say, what our customers say, special offer, special, restaurant, menu, food',
        ),
        'offers' => array(
            'display_name' => 'Special Offers',
            'title' => 'What special have we got for you!',
            'description' => null,
            'keywords' => 'offer, special offer, special, restaurant, menu, food',
        ),
    );

    public function getPage($name){
        $pageNames = Ezy::getPageNames();
        return $this->pages[strtolower($name)];
    }

}