<?php
namespace Ezy;
include_once('RecommendButton.php');
include_once('ReviewForm.php');
include_once('SpiceLevel.php');
include_once('Pagination.php');

abstract class ViewHelperBase {
    public $view;
    
    public function __construct() {        
        $this->view = new FakeViewCLass();
    }
 
	public function printPageDetails($page){
        return $this->printPageInfo($page);
    }
	public function printPageInfo($page){
		if ($page->getTitle() || $page->getDescription()) {
            $html = "<div class='page-info text-center'>";
            if ($page->getTitle()) $html .= "<h1 class='text-center title'>{$page->getTitle()}</h1>";
            if ($page->getDescription()) $html .= "<p class='description'>{$page->getDescription()}</p>";
			$html .= "</div>";
			return $html;
		}
    }
    
    private function _frameUrl($restaurant, $name) {
        $href = '';
        if (APPLICATION_ENV == 'development') {
            $href = "http://dev-local.ezy-server.com";
        } elseif (APPLICATION_ENV == 'testing') {
            $href = "https://dev.curryhunter.co.uk";
        } else {
            $href = CH_SWHOST;
        }

        return $href . "/restaurant/frames/$name/ref/" . $restaurant->getUrlFriendlyRef();
    }

    public function printOOFrame($restaurant) {
        if (!$restaurant->getOOEnabled()) return;
        if (!$restaurant->getOOLinkFromWebsite()) return;

        $src = $this->_frameUrl($restaurant, 'oo');
        return "<iframe src='$src' style='width:100%; height:250px;' scrolling='no' frameborder='0' >Order Online With CurryHunter.Com</iframe>";
    }
    
    public function printReservationButtonFrame($restaurant) {
        if (!$restaurant->getOOEnabled()) return;
        if (!$restaurant->getOOLinkFromWebsite()) return;
        if (!$restaurant->getShowPageReservation()) return;

        $src = $this->_frameUrl($restaurant, 'ro');
        return "<iframe src='$src' style='width:100%; height:100%;' scrolling='no' frameborder='0' >Order Online With CurryHunter.Com</iframe>";
    }
    
    public function pagination($feed) {
        return new \Ezy\Pagination($feed);
    }
    
    public function recommendButton($user, $restaurant = null) {
        return new \Ezy\RecommendButton($user, $restaurant);        
    }
    
    public function reviewForm() {
        return new \Ezy\ReviewForm();        
    }
    
    public function alert ($text, $classes = 'alert alert-warning') {
      return "<div class='text-center $classes'><br />$text <div class='clearfix'></div> <br /></div>";
    }
    
    public function alertWarning ( $text ) {
      return $this->alert( $text, 'alert alert-warning' );
    }
    
    public function alertInfo ( $text ) {
      return $this->alert ( $text, 'alert alert-info' );
    }
    
    public function alertDanger ( $text ) {
      return $this->alert ( $text, 'alert alert-danger' );
    }
    
    public function alertSuccess ( $text ) {
      return $this->alert ( $text, 'alert alert-success' );
    }
    
    public function spiceLevel($restaurant) {
        return new \Ezy\SpiceLevel($restaurant);       
    }
    
    public function getSeoMetaTags($seo) {
        // depricated
        return '';
    }
    
    public function getGaAnalyticsScript($seo) {
        //depricated
        return '';
    }
}

class FakeViewClass {
    public $base_url;   
    
    public function baseUrl($link) {
        if (! defined('PUBLIC_PATH')) {
            $link = str_replace('/tr/', "/" . basename(dirname(__DIR__)) ."/", $link);
        }
        
        return $this->base_url . $link;
    }
}
