<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Offer extends Base {

    public $title;    
    public $description;    
    public $show_on_scroll;
    public $value;
    public $valid_from;
    public $valid_until;
    public $priority;
    public $status;
    
    public $line1;
    public $line2;
    public $line3;
    public $line4;
    public $line5;  
    
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    public function isPublished(){
        if($this->getStatus() == Ezy::STATUS_PUBLISHED){
            if(null !== $this->getValidFrom() && null !== $this->getValidUntil()){
                return ($this->getValidFrom() <= date('Y-m-d') && $this->getValidUntil() > date('Y-m-d'));
            }
            
            return true;
        }
    }    
    
    public function setTitle($value){
        $this->title = $value;
        return $this;
    }
    public function getTitle(){
        return $this->escape($this->title);
    }
    
    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        return $this->description;
    }
    
    public function setLine1($value){
        $this->line1 = $value;
        return $this;
    }
    public function getLine1(){
        return $this->escape($this->line1);
    }

    public function setLine2($value){
        $this->line2 = $value;
        return $this;
    }
    public function getLine2(){
        return $this->escape($this->line2);
    }

    public function setLine3($value){
        $this->line3 = $value;
        return $this;
    }
    public function getLine3(){
        return $this->escape($this->line3);
    }

    public function setLine4($value){
        $this->line4 = $value;
        return $this;
    }
    public function getLine4(){
        return $this->escape($this->line4);
    }
    
    public function setLine5($value){
        $this->line5 = $value;
        return $this;
    }
    public function getLine5(){
        return $this->escape($this->line5);
    }
    
    public function setValidFrom($value){
        $this->valid_from = $value;
        return $this;
    }
    public function getValidFrom(){
        if($this->valid_from == '0000-00-00' || $this->valid_from == '') {
            $this->valid_from = null;
        }
        
        return $this->valid_from;
    }
    
    public function setValidUntil($value){
        $this->valid_until = $value;
        return $this;
    }
    public function getValidUntil(){
        if($this->valid_until == '0000-00-00' || $this->valid_until == '') {
            $this->valid_until = null;
        }
        return $this->escape($this->valid_until);
    }
    
    
    public function setImageId($value){
        $this->image_id = $value;
        return $this;
    }
    public function getImageId(){
        return $this->image_id;
    }
    public function getImage() {
        if (null !== $this->getImageId()) {
            $this->_image = new \Ezy\Photo(array(
                "id" => $this->getImageId(),
                "thumbnail_url" => "https://s3-eu-west-1.amazonaws.com/test.photos.ezy/{$this->getImageId()}__120_120.jpg",
                "image_url" => "https://s3-eu-west-1.amazonaws.com/test.photos.ezy/{$this->getImageId()}.jpg",
            ));
        }
        
        return $this->_image;
    }
    
    public function setShowOnScroll($value){
        $this->show_on_scroll = $value;
        return $this;
    }
    public function getShowOnScroll(){
        return $this->show_on_scroll;
    }
    
    public function setShowOnHome($value){
        $this->setShowOnScroll($value);
        return $this;
    }
    public function getShowOnHome(){
        return $this->getShowOnScroll();
    }
    
    public function setValue($value){
        $this->value = $value;
        return $this;
    }
    public function getValue(){
        return $this->value;
    }
    
    public function setPriority($value){
        $this->priority = $value;
        return $this;
    }
    public function getPriority(){
        return $this->escape($this->priority);
    }
    
    public function setStatus($value){
        if(strlen($value) == 1){
            $value = Ezy::getStatusByKey($value);
        }
        $this->status = $value;
        return $this;
    }
    public function getStatus(){
        return $this->status;
    }
}