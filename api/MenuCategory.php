<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

abstract class MenuCategory extends Base {

    public $name;
    public $image_id;
    public $type;   //{ classic , normal }
    public $description;
    public $priority;   
    public $status;
    
    private $_image;
    /**  
    * List of menu food_items
    * @see    MenuItem
    */
    public $food_items = array();
    
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    /*
    *   Get menu food items related to this category
    *   
    *
    */
    public function getFoodItems() {
        if (! $this->food_items instanceof Feed && is_array($this->food_items)) { 
            $this->food_items = new Feed($this->food_items);
        }
        
        return $this->food_items;
    }
    public function setFoodItems(Restaurant_Model_Feed $values){
        $this->food_items = $values;
        return $this;
    }
    
    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function getName(){
        return $this->escape($this->name);
    }
    
    public function setImageId($value){
        $this->image_id = $value;
        return $this;
    }
    public function getImageId(){
        return $this->image_id;
    }
    public function getImage() {
        if (null !== $this->getImageId()) {
            $this->_image = new \Ezy\Photo(array(
                "id" => "fakingID",
                "thumbnail_url" => "https://s3-eu-west-1.amazonaws.com/test.photos.ezy/76__120_120.jpg",
                "image_url" => "https://s3-eu-west-1.amazonaws.com/test.photos.ezy/76.jpg",
            ));
        }
        
        return $this->_image;
    }
    
    
    public function setType($value){
        $this->type = $value;
        return $this;
    }
    public function getType(){
        return $this->type;
    }
    
    
    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        return $this->escape($this->description);
    }
       
    
    public function setPriority($value){
        $this->priority = $value;
        return $this;
    }
    public function getPriority(){
        return $this->escape($this->priority);
    }    
        

    public function setStatus($value){
        if(strlen($value) == 1){
            $value = Ezy::getStatusByKey($value);
        }
        $this->status = $value;
        return $this;
    }
    public function getStatus($key = false){
        return $this->status;
    }
    public function getStatusName($key = false){
        return Ezy::getStatusNameByValue($this->getStatus());
    } 
}