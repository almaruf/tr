<?php 
namespace Ezy;

class RecommendButton {
    
    private $_user ;
    private $_to_show = false;
    
    public function __construct($user, $restaurant = null) {
        if (null !== $restaurant) {
            $this->_to_show = $restaurant->getWebsiteShowRecommendButton();
        }
   
        $this->_user = $user;
        return $this;
    }

    public function printButton($item) {
        return false;
        if (!$this->_to_show) {
            return false;
        }
        
        $nos = $item->getRecommended();
        $url = "#";
        
        if ($this->getUser() && $this->getUser()->isLoggedIn() && $item->getRecommendedByMe()) {
        
            if ($nos > 1) {
                $recStr = "You and " . ($nos-1) . " others recommended";
            } else {
                $recStr = "You recommended";
            }            
            $activeCLass = " disabled";
            
        } else {
            $recStr = ($nos > 0 ? "$nos recommendations" : "Recommend");
            $activeCLass = "ch-recommend-item";
            
            if (!$this->getUser() || !$this->getUser()->isLoggedIn()) {
                $oauth2Client = new \Apiv1_Client();
                $activeCLass = "";
                $url = $oauth2Client->getLoginUrl(array(
                    'target' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", 
                    'user_type' => 'normal'
                ));
            }
        }
        
        return "<a class='btn btn-default btn-small ch-recommend-customize $activeCLass' data-item-id='{$item->getId()}' href='$url'>
            <span class='glyphicon glyphicon-thumbs-up'></span><small> $recStr</small></a>";
    }
    
    public function getUser(){
        return $this->_user;
    }
}
