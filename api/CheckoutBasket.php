<?php 
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class CheckoutBasket extends Base {
    
    private $_session;
    private $_restaurant;
    
    public function orderBasket(Restaurant_Model_Restaurant $restaurant) {
        $this->_session = new Zend_Session_Namespace($restaurant::CHECKOUT_SESSION_NAMESPACE);
        $this->_restaurant = $restaurant;
    }
    
    public function printButton($item) {
        return "<a class='add-menu-item-button btn btn-default' id='{$category->getId()}' href='#{$category->getId()}'><span class=''>+</span></a>";
    }
    
    public function printBasketSmall() {
        $c = $this->_restaurant->getWebsiteCurrency();
        $html = "";
        
        $html .= "<script type='text/javascript'>";
        $html .= '$(\'.add-menu-item-button\').ajax(function(){
            url: ' . $this->_restaurant->getUrl('add-item-to-checkout-basket', 'ajax') . ', 
            success: function(){alert(\'success\');},
            error: function(){alert(\'failure\');}
        });';
        $html .= "</script>";
        
        $html .= "<table id='#ch-checkout-basket' class='table table-bordered'>";
        $html .= "<tr>";
            $html .= "<th>Category</th>";
            $html .= "<th>Item</th>";
            $html .= "<th>Price</th>";
            $html .= "<th>Quantity</th>";            
            $html .= "<th>Amount</th>";
        $html .= "<tr>";
        $html .= "<tbody id='#ch-checkout-basket-tbody'>";
        if ($this->session->orderLines) {
            foreach($this->session->orderLines as $line) {
                $html .= "<tr>";
                $html .= "<td>{$line->getCategory()}</td>";
                $html .= "<td>{$line->getName()}</td>";
                $html .= "<td>{$line->getPrice()}</td>";
                $html .= "<td>{$line->getQuantity()}</td>";
                $html .= "<td>" . ($line->getPrice() * $line->getQuantity()) . "</td>";
                $html .= "</tr>";
            }
        } else {
            $html .= "<tr>Checkout basket empty.</tr>";
        }
        
        $html .= "</tbody>";
        $html .= "<table>";
    }
}