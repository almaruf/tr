<?php 
$theme = getopt("t:");
$cs = getopt("c:");
If ($theme === false) {
    die("theme ref (t) - not provided\n");
} else {
    $theme = $theme['t'];
}

print("--- Compiling `$theme` related less files ---- \n");
If (empty($cs)) {
    print("color scheme (c) - not provided, compiling all files\n");
} else if (isset($cs['c'])) {
    $cs = $cs['c'];
}

require "lessc.inc.php";

$dir = __DIR__ . "/themes/ch/$theme/color_bank/";

if (!empty($cs)) {
    $less = new lessc;
    $less->addImportDir(__DIR__ . "/themes/ch/theme10/color_bank");
    $less->compileFile($dir . "$cs.less", $dir . "$cs.css");
} else {
    // compile all .less files except for color_scheme.less
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if ($file == "." || $file == ".." || $file == "color_scheme.less")
                    continue;

                if (endsWith($file, ".less")) {
                    $name = substr($file, 0, strlen($file) - 5); 
                    try {
                        print ("Compiling less file : $file \n");
                        $less = new lessc;
                        $less->addImportDir(__DIR__ . "/themes/ch/theme10/color_bank");
                        $less->compileFile($dir . "$name.less", $dir . "$name.css");
                    } catch (Exception $ex) {
                        echo "lessphp fatal error: ".$ex->getMessage() . "\n";
                    }
                }
            }
            closedir($dh);
        }
    }
}

function endsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}
