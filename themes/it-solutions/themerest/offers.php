<?php


class offers
{

    public function offerCategories(){
        return [
            [
                'id'=>'1',
                'name'=>'Category 1',
            ],
            [
                'id'=>'2',
                'name'=>'Category 2',
            ],            [
                'id'=>'3',
                'name'=>'Category 3',
            ]
        ];
    }

    public function offerInformations(){
        return [
            [
                'id'=>'1',
                'offer_category_id'=>'1',
                'name'=>'Offer Title 1',
                'description'=>'Interdum iusto pulvinar consequuntur augue optio repellat fugaurus expedita fusce temporibus malesio. ',
                'image_link'=>'https://s3-eu-west-1.amazonaws.com/test.photos.ezy/5__120_120.jpg',
                'price'=>'33',
                'currency'=>'$',
            ],

            [
                'id'=>'2',
                'offer_category_id'=>'2',
                'name'=>'Offer Title 2',
                'description'=>'Interdum iusto pulvinar consequuntur augue optio repellat fugaurus expedita fusce temporibus malesio. ',
                'image_link'=>'https://s3-eu-west-1.amazonaws.com/test.photos.ezy/5__120_120.jpg',
                'price'=>'33',
                'currency'=>'$',
            ],


            [
                'id'=>'3',
                'offer_category_id'=>'3',
                'name'=>'Offer Title 3',
                'description'=>'Interdum iusto pulvinar consequuntur augue optio repellat fugaurus expedita fusce temporibus malesio. ',
                'image_link'=>'https://s3-eu-west-1.amazonaws.com/test.photos.ezy/5__120_120.jpg',
                'price'=>'33',
                'currency'=>'$',
            ],
        ];

    }





}