<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
   public function printSpecialOffers($specialOffers, $restaurant = null, $homePage = null) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        $nosToPrint = 0;
        if ($homePage) {
            foreach($specialOffers->getItems() as $singleOffer) {
                if (!$singleOffer->getShowOnHome()) {
                    continue;
                }
                $nosToPrint++;
            }
        } else {
            $nosToPrint = $specialOffers->getTotal();
        }
        
        if ($nosToPrint == 1) {$classes = 'col-lg-12 col-md-12 col-sm-12col-xs-12 margin-15-auto-0 ';}
        elseif ($nosToPrint == 2) {$classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-15-auto-0 ';}
        else {$classes = 'col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-15-auto-0 ';}
        
        foreach($specialOffers->getItems() as $singleOffer) {
            if ($homePage && !$singleOffer->getShowOnHome()) {
                continue;
            }
            
            if (!$singleOffer->isPublished()) {
                continue;
            }
            
            $html .= "<div class='$classes sng_off_out' itemscope itemtype='http://schema.org/Offer'>";
            $html .= "<div class='sng_off'>";
                $html .= "<p class='ttl'><b><span itemprop='name'>{$singleOffer->getTitle()}</span></b></p>";
                if ($singleOffer->getImageId()){
                    $html .= "<img src='{$singleOffer->getImage()->getImageUrl()}' class='img-responsive hidden-xs hidden-sm' alt='{$singleOffer->getTitle()}'/>";
                } else {
                    $html .= "<img src='" . $this->view->baseUrl('/tr/themes/ch/theme8/img/fastfood2.png') . "' class='hidden-xs hidden-sm img-responsive' alt='' />";
				}
                
                $html .= "<ul class='list-unstyled'>";
                $oValue = (float)$singleOffer->getValue();
                if ($oValue) {
                    $html .= "<li>";
                        if ($restaurant) {
                            $html .= "<span class='priceCurrency' itemprop='priceCurrency' content='{$restaurant->getWebsiteCurrency()}'>{$restaurant->getCurrencySymbol()}</span>";
                        }
                        $html .= "<span class='price' itemprop='price'>{$singleOffer->getValue()}</span>";
                    $html .= "</li>";
                }
                
                $html .= "<span itemprop='description'>";
                
                if ( $singleOffer->getDescription() ) {
                    $html .= "<li>{$singleOffer->getDescription()}</li>";
                }
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li>{$singleOffer->getLine1()}</li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li>{$singleOffer->getLine2()}</li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li>{$singleOffer->getLine3()}</li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li>{$singleOffer->getLine4()}</li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li>{$singleOffer->getLine5()}</li>";
                }
                
                $html .= "</span>";
                $html .= "</ul>";
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= ($address->getCountry() ? "<div itemprop='addressCountry'>{$address->getCountry()}</div>" : "");        
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        $html  = '';
        $html .= ($address->getDoorNo() ? "<h5 class=''>{$address->getDoorNo()}" : "");
        $html .= ($address->getHouseName() ? " - {$address->getHouseName()}" : " - ");
        $html .= ($address->getRoad() ? "{$address->getRoad()}" : "");
        $html .= ($address->getTown() ? "<br />{$address->getTown()}</h5>" : "");
        $html .= ($address->getCity() ? "<h5 class=''>{$address->getCity()}" : "<h5 class=''>");
        $html .= ($address->getPostcode() ? "{$address->getPostcode()}" : "");
        $html .= ($address->getCounty() ? "<br />{$address->getCounty()}" : "");
        $html .= ($address->getCountry() ? ", {$address->getCountry()}</h5>" : "");        
        return $html;
    }
	
	public function printPageDetails($page){
		if ($page->getTitle() || $page->getDescription()) {
            $html = "<div class='page-info text-center'>";
            if ($page->getTitle()) $html .= "<h1 class='text-center title'>{$page->getTitle()}</h1>";
            if ($page->getDescription()) $html .= "<p class='description'>{$page->getDescription()}</p>";
			$html .= "</div>";
			return $html;
		}
	}
    
}
