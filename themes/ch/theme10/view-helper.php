<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
    public function printPhoneNumbers($restaurant) {
        $phone_number  = '';
        if ($restaurant->getPhoneReservation()) {
            $phone_number .= '<span>Takeaway :</span>&nbsp;&nbsp;';
            $phone_number .= "<a class='another_color' href='tel:{$restaurant->getPhoneReservation()}'>";
            $phone_number .= "{$restaurant->getPhoneReservation()}</a>&nbsp;";
        } elseif ($restaurant->getPhoneTakeaway()) {
            $phone_number .= '<span>Reservation :</span>&nbsp;&nbsp;';
            $phone_number .= "<a class='another_color' href='tel:{$restaurant->getPhoneTakeaway()}'>";
            $phone_number .= "{$restaurant->getPhoneTakeaway()}</a>";
        } else {
            $phone_number = 'XXXXXXXXXXXXXXXX';
        }
                
        return "<p>$phone_number</p>";
    }
    
    public function printSpecialOffers($specialOffers, $restaurant = null, $homePage = null) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        $nosToPrint = 0;
        if ($homePage) {
            foreach($specialOffers->getItems() as $singleOffer) {
                if (!$singleOffer->getShowOnHome()) {
                    continue;
                }
                $nosToPrint++;
            }
        } else {
            $nosToPrint = $specialOffers->getTotal();
        }
        
        if ($nosToPrint == 1) {$classes = 'col-lg-12 col-md-12 col-sm-12col-xs-12 text-center';}
        elseif ($nosToPrint == 2) {$classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';}
        else {$classes = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';}
        
        foreach($specialOffers->getItems() as $singleOffer) {
            if ($homePage && !$singleOffer->getShowOnHome()) {
                continue;
            }
            
            if (!$singleOffer->isPublished()) {
                continue;
            }
            
            $html .= "<div class='$classes single-offer' itemscope itemtype='http://schema.org/Offer'>";
            $html .= "<table class='table table-bordered'><tr><td>";
            
                $html .= "<p class='title theme_color'><b><span itemprop='name'>{$singleOffer->getTitle()}</span></b></p>";
                if ($singleOffer->getImageId()) {
                    $html .= "<img src='{$singleOffer->getImage()->getImageUrl()}' class='img-responsive' alt='{$singleOffer->getTitle()}' />";
                }
                $html .= "<ul class='list-unstyled'>";
                $oValue = (float)$singleOffer->getValue();
                if ( $oValue ) {
                    $html .= "<li>";
                        if ($restaurant) {
                            $html .= "<span class='priceCurrency theme_color' itemprop='priceCurrency' content='{$restaurant->getWebsiteCurrency()}'>{$restaurant->getCurrencySymbol()}</span>";
                        }
                        $html .= "<span class='price theme_color' itemprop='price'>{$singleOffer->getValue()}</span>";
                    $html .= "</li>";
                }
                
                $html .= "<span itemprop='description'>";
                
                if ( $singleOffer->getDescription() ) {
                    $html .= "<li>{$singleOffer->getDescription()}</li>";
                }
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li>{$singleOffer->getLine1()}</li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li>{$singleOffer->getLine2()}</li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li>{$singleOffer->getLine3()}</li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li>{$singleOffer->getLine4()}</li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li>{$singleOffer->getLine5()}</li>";
                }
                
                $html .= "</span>";
                $html .= "</ul>";
            $html .= "</td></tr></table>";
            $html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCountry() ? "<div itemprop='addressCountry'>{$address->getCountry()}</div>" : "");        
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        ($address->getDoorNo() ? $parts[] = "{$address->getDoorNo()}" : null);
        ($address->getRoad() ? $parts[] = "{$address->getRoad()}" : null);
        ($address->getHouseName() ? $parts[] = "{$address->getHouseName()}" : null);
        ($address->getTown() ? $parts[] = "{$address->getTown()}" : null);
        ($address->getCity() ? $parts[] = "{$address->getCity()}" : null);
        ($address->getCounty() ? $parts[] = "{$address->getCounty()}" : null);
        ($address->getPostcode() ? $parts[] = "{$address->getPostcode()}" : null);
        
        return implode(", ", $parts);
    }
	
	public function printPageDetails($page){
		if( $page->getTitle() ){
			$html = "";
			$html .= "<div class='page-info col-md-12 text-center'>";
			$html .= "<h1 class='title'>{$page->getTitle()}</h1>";
			if( $page->getDescription() ){
				$html .= "<p class='description'>{$page->getDescription()}</p>";
			}
			$html .= "</div>";
			return $html;
		}
	}
    
}
