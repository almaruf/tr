$(document).ready(function() {
   $('#nav').slicknav();
   $(".fancybox").fancybox();
   
   //scroll to top
   $("a[href='#top']").click(function(){
	$("html, body").animate({scrollTop: 0}, "slow");
	return false;
   });
});
