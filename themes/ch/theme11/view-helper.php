<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
    public function printPhoneNumbers($restaurant) {
        $phone_number  = '';
        if ($restaurant->getPhoneReservation()) {
            $phone_number .= '<span>Takeaway :</span>&nbsp;&nbsp;';
            $phone_number .= "<a class='another_color' href='tel:{$restaurant->getPhoneReservation()}'>";
            $phone_number .= "{$restaurant->getPhoneReservation()}</a>&nbsp;";
        } elseif ($restaurant->getPhoneTakeaway()) {
            $phone_number .= '<span>Reservation :</span>&nbsp;&nbsp;';
            $phone_number .= "<a class='another_color' href='tel:{$restaurant->getPhoneTakeaway()}'>";
            $phone_number .= "{$restaurant->getPhoneTakeaway()}</a>";
        } else {
            $phone_number = 'XXXXXXXXXXXXXXXX';
        }
                
        return "<p>$phone_number</p>";
    }
    
    public function printSpecialOffers($specialOffers, $restaurant = null, $homePage = null) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        $nosToPrint = 0;
        if ($homePage) {
            foreach($specialOffers->getItems() as $singleOffer) {
                if (!$singleOffer->getShowOnHome()) {
                    continue;
                }
                $nosToPrint++;
            }
        } else {
            $nosToPrint = $specialOffers->getTotal();
        }
        
        if ($nosToPrint == 1) {$classes = 'col-lg-12 col-md-12 col-sm-12col-xs-12';}
        elseif ($nosToPrint == 2) {$classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';}
        else {$classes = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';}
        
        foreach($specialOffers->getItems() as $singleOffer) {
            if ($homePage && !$singleOffer->getShowOnHome()) {
                continue;
            }
            
            if (!$singleOffer->isPublished()) {
                continue;
            }
            
            $html .= "<div class='$classes'>";
            $html .= "<div class='panel panel-default'>";
			$html .= "<div class='panel-body'>";
                $html .= "<h4>{$singleOffer->getTitle()}</h4>";
                $oValue = number_format((float)$singleOffer->getValue(), 2);
                if ($oValue) {
                    $html .= "<h5>{$restaurant->getCurrencySymbol()}{$oValue}</h5>";
                }
                if ( $singleOffer->getDescription() ) {
                    $html .= $singleOffer->getDescription();
                }
               
                $html .= "<ul class='list-unstyled'>";
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li>{$singleOffer->getLine1()}</li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li>{$singleOffer->getLine2()}</li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li>{$singleOffer->getLine3()}</li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li>{$singleOffer->getLine4()}</li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li>{$singleOffer->getLine5()}</li>";
                }
                $html .= "</ul>";

            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        ($address->getDoorNo() ? $parts[] = "{$address->getDoorNo()}" . ($address->getRoad() ? " - {$address->getRoad()}" : null) : null);
        ($address->getHouseName() ? $parts[] = "{$address->getHouseName()}" : null);
        ($address->getTown() ? $parts[] = "{$address->getTown()}" : null);
        ($address->getCity() ? $parts[] = "{$address->getCity()}" : null);
        ($address->getPostcode() ? $parts[] = "{$address->getPostcode()}" : null);
        ($address->getCounty() ? $parts[] = "{$address->getCounty()}" : null);
        
        return implode(", ", $parts);
    }
	
	public function printPageDetails($page){
		if( $page->getTitle() ){
			$html = "";
			$html .= "<div class='page-info col-md-12 text-center'>";
			$html .= "<h1 class='title'>{$page->getTitle()}</h1>";
			if( $page->getDescription() ){
				$html .= "<p class='description'>{$page->getDescription()}</p>";
			}
			$html .= "</div>";
			return $html;
		}
	}
    
}
