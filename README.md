# Theme development 
* Step 1: Clone this repository git@bitbucket.org:almaruf/tr.git in your local environment.

* Step 2: Create a directory with a simple name that identifies your company or your name in /themes directory. You will have to put the same name on the config.php file as the value of 'developer_repo' key for every theme you create from now on.

* Step 3: Add a new array item on the config.php file with an unique index as 'ref' ie. theme5 to identify your theme. Theme names are different, so keep the 'ref' value short and concise that only identifies your theme internally at code level. An explanation on the array keys can found on config.php file itself.

* Step 4: For a easy start copy an existing theme ie. theme1 from /themes/ch directory onto your development directory created on 'Step 2'. Rename the copied directory to match the 'ref' you created for your theme at 'Step 3'. /themes/ch is the directory reserved for CurryHunter developers.

* Step 5: Browse the /api directory or [see the documentation](https://dev.curryhunter.co.uk/tr/api/apidocs/) for reference to the PHP objects 

* Step 6: A theme requires few files to be created. They are :
````
view-helpers.php
layout.phtml
index.phtml 
menu.phtml
reviews.phtml
photos.phtml
videos.phtml
about.phtml
```` 
please create these files and use the restaurant object to print information on these pages. An example of what you can do with the restaurant object is provided below. 

On layout.phtml file, try this code block
````
<?php 

echo $this->restaurant->getName() 

$menu = $this->restaurant->getMenu();
$categories = $menu->getNormalCategories();

if (!empty($categories)) {
   foreach($categories->getItems() as $category) {
       echo $category->getName();
       echo $category->getDescription();
   }
}
?>
````
Please have a look at the [api documentation](https://dev.curryhunter.co.uk/tr/api/apidocs/) to get a full list of available classes and their member functions.
 
* Step 7: Keep the value of the 'status' key to 'testing' during development. The CurryHunter moderator will change it when going live.

# What's next?

# Learning 
* How to make [mobile friendly theme](https://developers.google.com/web/fundamentals/layouts/rwd-fundamentals)
* How to let google know specific [things in a theme](https://developers.google.com/structured-data/rich-snippets) ie. review, menu.